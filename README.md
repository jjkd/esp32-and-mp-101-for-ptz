This project adds computerized control of the Bescor MP-101 Pan/Tilt head through the use of an Espressif ESP32-based module and a purpose-built interface to drive the MP-101.  Closed-loop operation is supported through the application of encoders to provide feedback for the pan and tilt axies, but these can be omitted if closed-loop operation/automation is not required and only manual remote control is to be used.  LANC support is also provided for a suitable camera, though no feedback/closed-loop capability for zoom is implemented.  This project includes 3D-printed components, custom electronics and supporting software.

The current iteration looks like this:

![](images/IMG_6844.JPG)

[More pictures and details will be in the Wiki.](https://gitlab.com/jjkd/esp32-and-mp-101-for-ptz/-/wikis/Overview-of-the-add-ons-installed-on-the-MP-101)

Here is a video showing the default power-up sequence for the prototype firmware.

![](videos/PowerUp.mp4)

The sequence shown in the video includes:

- Hardware initialization
- WiFi network acquisition
- Tilt calibration
- Pan calibration
- Seek to 'home' position

Here is the orginal 'proof-of-concept' rig used to determine whether driving the MP-101 interface from software with the ESP32 was practical:

![](images/IMG_6836.JPG)

The inteface to the MP-101 has been implemented in three modules.  The first module is an off-the-shelf ESP32 module from Heltec:

![](images/IMG_6835.JPG)

This device was chosen based on generally good reviews, substantial documentation and the use of the display for debugging and status information. You can see this module in the overall view, plugged into the next module and sitting horizontally on the 'back shelf' of the MP-101 below the camera dovetail slide. 

The second module is the ESP32 module carrier board:

![](images/IMG_6833.JPG)

This is a very simple board, and only serves to map the interface signals used for the ESP32 module.  Again, you can see this module/board sitting horizontally on the 'back shelf' of the MP-101 below the camera dovetail slide. 

Because many different ESP32 modules are available and there does not seem to be a standardized pinout, I implemented this mapping functionality as a separate board that can be relatively easily customized for any particular ESP32 module. This could be done by rerouting the board and producing a new version, or by simply cutting and jumpering traces on the back of the board.

Finally, the third module is the MP-101 interface board. You can see this module plugged in to the 7 pin DIN connector on the 'back side' of the MP-101.  This module is responsible for translating the 3.3 volt logic level signalling used by the ESP32 module to meet the requirements of the MP-101.  It also provides similar translation for the LANC interface both in terms of the voltage levels, and also by hosting the [ELM 624 LANC to RS232 interpreter chip](https://www.elmelectronics.com/ic/elm624/) (to be installed in the empty socket shown in the detail view here, you can see this chip installed on the module in the overview picture).

![](images/IMG_6834.JPG)

I have considered producing a second version that uses a single PC board, there is sufficient room once the JST XH connectors are removed, however that would make any changes required as a result of using a different ESP32 module more difficult.  The big pluses would be to avoid having to crimp all those JST XH jumpers, the reduced number of potential connection faults, and the cost of the connectors.