// const url = 'wss://myserver.com/something'
const url = 'ws://192.168.1.120:8080/'
const connection = new WebSocket(url)


function sendCommand(arg) {

    if ((arg == "") || (arg == "NoCommand")) {
        return;
    }

    console.log("Command to be sent was: " + arg);

    connection.send(arg);    
}

connection.onopen = () => {
    console.log('WebSocket opened!');
  }

connection.onerror = error => {
    console.log(`WebSocket error: ${error}`);
    
  }

  connection.onmessage = e => {
    handle_incoming(e.data);
    console.log(e.data);
  }


  function handle_incoming(data) {
   
      var message = data.split(":");

      switch (message[0]) {
        case "opmode":
          document.getElementById('op_mode').innerHTML = message[1];
        break;

        case "pan":
          document.getElementById('pan_location').innerHTML = message[1];
        break;

        case "tilt":
          document.getElementById('tilt_location').innerHTML = message[1];
        break;

        case "pan_t":
          document.getElementById('pan_target').innerHTML = message[1];
        break;

        case "tilt_t":
          document.getElementById('tilt_target').innerHTML = message[1];
        break;
        default:

      }

  }